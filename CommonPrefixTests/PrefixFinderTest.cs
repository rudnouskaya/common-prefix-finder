using CommonPrefix;

namespace LongestCommonPrefixTests
{
    public class Tests
    {
        [Test]
        [TestCase(new string[] { "flower", "flow", "flight" }, "fl")]
        [TestCase(new string[] { "abc", "abcd", "abcde" }, "abc")]
        [TestCase(new string[] { "word" }, "word")]
        [TestCase(new string[] { "", "" }, "")]
        public void FindLongestCommonPrefix_ValidInput_ReturnsCommonPrefix(string[]input, String expected)
        {
            // Act
            string result = PrefixFinder.FindLongestCommonPrefix(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(new string[] { "dog", "racecar", "car" }, "")]
        public void FindLongestCommonPrefix_StringsWithoutCommonPrefix_ReturnsEmptyString(string[] input, String expected)
        {
            // Act
            string result = PrefixFinder.FindLongestCommonPrefix(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("flower,dog", true)]
        public void IsValidInput_StringsWithValidCharacters_ReturnsTrue(string input, Boolean expected)
        {
            // Act
            bool result = PrefixFinder.IsValidInput(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(",", true)]
        public void IsValidInput_EmptyStrings_ReturnsTrue(string input, Boolean expected)
        {
            // Act
            bool result = PrefixFinder.IsValidInput(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("flower", true)]
        public void IsValidInput_SingleValidString_ReturnsTrue(string input, Boolean expected)
        {
            // Act
            bool result = PrefixFinder.IsValidInput(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(null, false)]
        public void IsValidInput_NullInput_ReturnsFalse(string input, Boolean expected)
        {
            // Act
            bool result = PrefixFinder.IsValidInput(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("ab/cd", false)]
        [TestCase("ab,C", false)]
        [TestCase("  ,  ", false)]
        public void IsValidInput_InvalidCharacters_ReturnsFalse(string input, Boolean expected)
        {
            // Act
            bool result = PrefixFinder.IsValidInput(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("a,twentyonecharactersss", false)]
        public void IsValidInput_StringLengthAboveMax_ReturnsFalse(string input, Boolean expected)
        {
            // Act
            bool result = PrefixFinder.IsValidInput(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,", false)]
        public void IsValidInput_StringsCountAboveMax_ReturnsFalse(string input, Boolean expected)
        {
            // Act
            bool result = PrefixFinder.IsValidInput(input);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }
    }
}
