## Common Prefix Finder
Common Prefix Finder is a console application that finds common prefix amongst an array of strings.

## Original assignment
Using C#, please write a working solution:
**to find the longest common prefix string amongst an array of strings. If there is no common prefix, return an empty string "".**

Please write working unit tests against your solution and push it to the GIT repository.
If something is not specified in the requirements below, you can decide for yourself.

**Example 1:**
Input: strs = ["flower","flow","flight"]
Output: "fl"

**Example 2:**
Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

Constraints:
1 <= strs.length <= 200
0 <= strs[i].length <= 20
strs[i] consists of only lowercase English letters.

## Prerequisites
Visual Studio 2019 or higher

## Running the application
Run the CommonPrefix project (F5 from Visual Studio).
Run the tests (Ctrl+R,A from Visual Studio).
