﻿
using CommonPrefix;
using System;
using System.Text.RegularExpressions;

class ProgramDriver
{



    public static void Main()
    {
        PrintWelcomeMessage();
        string input = Console.ReadLine();

        //print an error and stop the program if the input is invalid
        if (!PrefixFinder.IsValidInput(input))
        {
            Console.WriteLine("Input is invalid. Please read the rules and try again.");
            return;
        }

        //create an array of strings from the input
        string[] strs = input.Split(',');

        //find the common prefix and store it in a string
        string longestCommonPrefix = PrefixFinder.FindLongestCommonPrefix(strs);

        //print the result
        Console.WriteLine($"The longest common prefix is: \"{longestCommonPrefix}\"");
    }

    private static void PrintWelcomeMessage()
    {
        Console.WriteLine("==============================================================================================");
        Console.WriteLine("Welcome to the CommonPrefixFinder, which helps you find out whether a number of strings have a common prefix.");
        Console.WriteLine("==============================================================================================");
        Console.WriteLine("The rules are: \n 1. You can enter 1-200 strings separated by comma, *no spaces*. \n 2. Each string can be 0-20 characters long. \n 3. Each string must consist of only lowercase English letters \n    Example of the input: drum,drill,dragon,diamond \n 4.Press Enter when done");
        Console.WriteLine("==============================================================================================");
    }


}
