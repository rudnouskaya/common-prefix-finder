﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace CommonPrefix
{
    public class PrefixFinder
    {
        private const string AllowedCharacters = "^[a-z,]*$";
        private const int MaxStringsCount = 200;
        private const int MaxStringLength = 20;
        public static bool IsValidInput(string input)
        {
            if (input == null)
            {
                return false;
            }

            // check if the input contains invalid characters
            if (!Regex.IsMatch(input, AllowedCharacters))
            {
                return false;
            }

            string[] strs = input.Split(',');

            // check if the input contains more strings than allowed
            if (strs.Length > MaxStringsCount)
            {
                return false;
            }

            // check if any string in the inout is longer than allowed
            foreach (string s in strs)
            {
                if (s.Length > MaxStringLength)
                {
                    return false;
                }
            }

            return true;
        }
        public static string FindLongestCommonPrefix(string[] strs)
        {
            //if less than 2 strings in the array - return the whole string 
            if (strs.Length <= 1)
            {
                return strs[0];
            }

            //loop through the array to find the shortest string and set it as the max prefix length
            int maxPrefixLen = strs[0].Length;
            foreach (string str in strs)
            {
                maxPrefixLen = Math.Min(maxPrefixLen, str.Length);
            }

            //loop through each character of the 1st string of the array up to maxPrefixLen-1 character
            for (int i = 0; i < maxPrefixLen; i++)
            {
                char c = strs[0][i];

                //loop through each string in the array and compare its characters to the characters of the 1st string in the same position
                for (int j = 1; j < strs.Length; j++)
                {
                    //stop the loop when the characters don't match
                    if (strs[j][i] != c)
                    {
                        //return common prefix in all strings 
                        return strs[0].Substring(0, i);
                    }
                }
            }
            return strs[0].Substring(0, maxPrefixLen);
        }
    }
}